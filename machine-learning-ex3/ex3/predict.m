function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

% Initialize A2 and A3, whose rows will store activations for each row of X
% at 2nd and 3rd levels respectively.

A2 = zeros(m, size(Theta1,1));
A3 = zeros(m, size(Theta2,1));

% Add column of 1s to X
X = [ones(m,1), X];

% Stores the level 2 activation states for each row of X
A2 = sigmoid(X*(Theta1'));

% Adds column of ones to A2
A2 = [ones(m,1), A2];

% Stores final level 3 probabilites for each num_label in each rows
A3 = sigmoid( A2*(Theta2'));

[w,iw] = max(A3');
p = iw'







% =========================================================================


end
