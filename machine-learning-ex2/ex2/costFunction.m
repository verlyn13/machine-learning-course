function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%
% Initialize vector of sigmoid inputs, outputs, and log of sigmoid outputs
z = zeros(m,1);
v = zeros(m,1);
vlog = zeros(2*m,1);

% Create vector of z-values
for i = [1:1:m] ;
  z(i) = X(i,:)*theta;
endfor

% Create vector of sigmoid of z-values and one minus sigmoid of z-values
v = sigmoid(z);
vlog = log([sigmoid(z);(1 - sigmoid(z))]);

% Finds cost function
J = (-1/m)*([(y'),(1-(y'))]*vlog);

% Finds the gradiant of J
grad = (1/m)*((X')*(v-y));









% =============================================================

end
