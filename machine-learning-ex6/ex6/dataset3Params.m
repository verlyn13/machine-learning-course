function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and
%   sigma. You should complete this function to return the optimal C and
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example,
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using
%        mean(double(predictions ~= yval))
%

errors = zeros(8,8)
poss_C = [0.01,0.03,0.1, 0.3, 1, 3, 10, 3];
poss_sigma = [0.01,0.03,0.1, 0.3, 1, 3, 10, 3];

for i=1:8;
  for j=1:8;
        temp_model = svmTrain(X, y, poss_C(i), @(x1, x2) gaussianKernel(x1, x2, poss_sigma(j)));
        temp_pred = svmPredict(temp_model,Xval);
        errors(i,j) = mean(double(temp_pred~=yval));
  end
end

[M,I] = min(errors(:));
[best_i , best_j] = ind2sub(size(errors),I);
C = poss_C(best_i);
sigma = poss_sigma(best_j);


% =========================================================================

end
