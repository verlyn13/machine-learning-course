function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m

% Initialize some useful vectors


z2 = zeros(hidden_layer_size,m);
z3 = zeros(num_labels,m);
a2 = zeros(hidden_layer_size,m);
a3 = zeros(num_labels,m);
log_a3 = zeros(num_labels,m);
log_one_minus_a3 = zeros(num_labels,m);
D1 = zeros(size(Theta1));
D2 = zeros(size(Theta2));


% Add column of ones in front of X
X = [ones(m,1), X];

% Creates matrix where element k,i is 1 if ith training example is classified 
% as k, 0 otherwise
ybin = [1:num_labels == y]; 

z2 = Theta1*(X') ;
a2 = sigmoid(z2) ;

% Add bias +1 to a2 for next propogation
a2 = [ones(1,m);a2];

% Computes final layer's activations
z3 = Theta2*a2;
a3 = sigmoid(z3);

% Finds the log of a3 activations
log_a3 = log(a3);
log_one_minus_a3 = log(1-a3); 

% Cost function
J = (-1/m)*(trace(ybin*log_a3) + trace((1-ybin)*log_one_minus_a3));

% Unroll theta not including bias parameters
Theta1_no_bias = Theta1(:,(2:end));
Theta2_no_bias = Theta2(:,(2:end));
Theta_unrolled = [Theta1_no_bias(:);Theta2_no_bias(:)];

J = J + (lambda/(2*m))*sum(Theta_unrolled.^2)




%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%

for t=1:m
  bp_a1 = (X')(:,t);
  bp_z2 = Theta1*bp_a1 ;
  bp_a2 = sigmoid(bp_z2) ;
  % Append bias node to a2 activation layer
  bp_a2 = [1;bp_a2];
  bp_z3 = Theta2*bp_a2;
  bp_a3 = sigmoid(bp_z3);
  % Forward feed done, now backpropogating errors
  delta3 = (bp_a3 - (ybin')(:,t));
  delta2 = ((Theta2')*delta3).*bp_a2.*(1-bp_a2);
  delta2 = delta2(2:end);
  Theta2_grad = Theta2_grad + delta3*(bp_a2');
  Theta1_grad = Theta1_grad + delta2*(bp_a1');
endfor

% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% Put together averaged accumulated backpropogated errors with no regularlization
% in the bias terms
%reg_grad1 = [zeros(
%reg_grad2 = [zeros(num_labels,1), 

D1 = [(1/m)*Theta1_grad(:,1) , (1/m)*Theta1_grad(:,2:end) + (lambda/m)*Theta1(:,2:end)];
D2 = [(1/m)*Theta2_grad(:,1) , (1/m)*Theta2_grad(:,2:end) + (lambda/m)*Theta2(:,2:end)];

% Unroll the gradients
grad = [D1(:);D2(:)];

% -------------------------------------------------------------




end
