function centroids = computeCentroids(X, idx, K)
%COMPUTECENTROIDS returns the new centroids by computing the means of the
%data points assigned to each centroid.
%   centroids = COMPUTECENTROIDS(X, idx, K) returns the new centroids by
%   computing the means of the data points assigned to each centroid. It is
%   given a dataset X where each row is a single data point, a vector
%   idx of centroid assignments (i.e. each entry in range [1..K]) for each
%   example, and K, the number of centroids. You should return a matrix
%   centroids, where each row of centroids is the mean of the data points
%   assigned to it.
%

% Useful variables
[m n] = size(X);

% You need to return the following variables correctly.
centroids = zeros(K, n);


% ====================== YOUR CODE HERE ======================
% Instructions: Go over every centroid and compute mean of all points that
%               belong to it. Concretely, the row vector centroids(i, :)
%               should contain the mean of the data points assigned to
%               centroid i.
%Program paused. Press ent
% Note: You can use a for-loop over the centroids to compute this.
%
sum = zeros(1,n);
a = zeros(size(idx,1),1);
l = 1
for i=1:K
  a = find(idx == i);
  l = length(a);
  if (l==0)
    centroids(i,:) = zeros(1,n);
  else
  for j=1:l
    sum = sum + X(a(j),:);
  end
  centroids(i,:) = (1/l)*sum;
  endif
  sum = zeros(1,size(centroids,2));
  a = zeros(size(idx,1),1);
  l = 1
end


% =============================================================


end
